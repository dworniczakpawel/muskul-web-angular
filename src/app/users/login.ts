export interface Login {
  logged: boolean;
  token: string;
}
